local cmd = vim.cmd
local exec = vim.api.nvim_exec
local g = vim.g
local opt = vim.opt

opt.spelllang={ 'en_us', 'ru' }
opt.number=true
opt.splitright=true
opt.splitbelow=true
opt.clipboard:append {unnamedplus}
opt.keymap='russian-jcukenwin'
-- opt.guifont='Iosevka:h18'
opt.termguicolors=true
-- cmd'colorscheme tokyonight'
cmd([[
filetype indent plugin on
syntax enable
]])
opt.expandtab=true
opt.shiftwidth=4
opt.softtabstop=4
opt.smartindent=true
opt.wrap=true
opt.iminsert=0
opt.imsearch=0
opt.foldcolumn='4'
opt.lbr=true
exec([[
augroup YankHighlight
autocmd!
autocmd TextYankPost * silent! lua vim.highlight.on_yank{higroup="IncSearch", timeout=700}
augroup end
]], false)
-- vim.g['loaded_netrw'] = 1
-- vim.g['loaded_netrwPlugin'] = 1

vim.g['pandoc#syntax#conceal#urls'] = 1
vim.g['pandoc#formatting#mode'] = 's' 
vim.g['pandoc#syntax#style#emphases'] = 1
vim.g['pandoc#syntax#codeblocks#embeds#langs'] = { "lua", "sh", "tex", "vim" }
vim.g['pandoc#folding#fdc']=4
vim.g['vimtex_view_method'] = 'zathura'

-- require('lualine').setup()
