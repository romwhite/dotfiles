local my_shortcut = {
    { 
        icon = ' ',
        desc = 'My morning pages',
        key = 'm',
        action = 'e ~/FreeBSD_Sync/morning2021.md'
},
    {
        desc = 'NeoVim config',
        key = 'n',
        action = 'Neotree ~/.config/nvim/'
    },
}

return {
  'nvimdev/dashboard-nvim',
  event = 'VimEnter',
  config = function()
    require('dashboard').setup {
      -- config
      theme = 'hyper',
      config = {
      week_header = { enable = true },
      shortcut = my_shortcut,
      project = { enable = false }
          },
      }
end,
  dependencies = { {'nvim-tree/nvim-web-devicons'}}
}
