return {
    'nvim-lualine/lualine.nvim',
-- Now don't forget to initialize lualine
    config = function()
        require('lualine').setup {
            options = { disabled_filetypes = {'neo-tree'} }
        }
    end,
    dependencies = { 'nvim-tree/nvim-web-devicons' }
}



