;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
(setq user-full-name "Romaha"
      user-mail-address "romwhite@gmail.com")
(setq tab-bar-mode nil)
;;(set-language-environment "Russian")
(prefer-coding-system 'utf-8)
;;(setq doom-font (fonts-spec :family "JetBrains Mono" :size 16))
;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
(setq  doom-theme 'doom-gruvbox
       doom-font (font-spec :family "JetBrains Mono" :size 14.0)
       ;; doom-font (font-spec :family "FantasqueSansMono" :size 16.0)
        doom-variable-pitch-font (font-spec :family "JetBrains Mono" :size 14.0)
       )
(after! doom-theme
  (setq doom-themes-enable-bold t
        doom-themes-enable-italic t))

;;Needed for building telega.el
(setq telega-server-libs-prefix "~/Soft/td/tdlib")

(setq org-directory "~/Yandex.Disk/org/")

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-mode 1)

(global-set-key "\C-xh" 'help-command)
;;(keyboard-translate ?\C-h ?\C-?)
(keyboard-translate ?\C-a ?\C-\\)

(set-language-environment "Russian")

(prefer-coding-system 'utf-8)
(global-set-key "\C-a" 'toggle-input-method)
;;(setq ispell-program-name "hunspell")
  ;;(ispell-hunspell-add-multi-dic "ru_RU, en_US-large"))

(add-hook 'spell-fu-mode-hook
          (lambda()
            (spell-fu-dictionary-add(spell-fu-get-ispell-dictionary "ru-yo"))
            (spell-fu-dictionary-add(spell-fu-get-ispell-dictionary "en_US"))
            ))

(after! org
  (setq org-directory "~/Yandex.Disk/org/"
        display-line-numbers-type nil
        org-agenda-files '("~/Yandex.Disk/org/plans.org")
        org-odt-styles-file "~/Yandex.Disk/org/default.ott"
        org-default-notes-file (expand-file-name "notes.org" org-directory)
        org-ellipsis " ▼ "
        org-log-done 'time
        org-journal-dir "~/Yandex.Disk/org/journal/"
        org-journal-date-format "%B %d, %Y (%A) "
        org-journal-file-format "%Y-%m-%d.org"
        org-hide-emphasis-markers t
        ;; ex. of org-link-abbrev-alist in action
        ;; [[arch-wiki:Name_of_Page][Description]]
        org-link-abbrev-alist    ; This overwrites the default Doom org-link-abbrev-list
          '(("google" . "http://www.google.com/search?q=")
            ("arch-wiki" . "https://wiki.archlinux.org/index.php/")
            ("ddg" . "https://duckduckgo.com/?q=")
            ("wiki" . "https://en.wikipedia.org/wiki/")))
  )

(emms-default-players)
(emms-mode-line 1)
(emms-playing-time 1)
(setq emms-source-file-default-directory "/mnt/wdred/Music"
      emms-playlist-buffer-name "*Music*"
      emms-info-asynchronously t
      emms-source-file-directory-tree-function 'emms-source-file-directory-tree-find)

;;Ya-snippet section
(setq yas-snippet-dirs (append yas-snippet-dirs '("~/.emacs.d/snippets/org-mode")))
(setq yas-snippet-dirs (append yas-snippet-dirs '("~/.emacs.d/snippets/markdown-mode")))
(map! :map markdown-mode-map
      :i "M-j" #'yas-next-field-or-maybe-expand )

(map! :n "j" #'evil-next-visual-line
      :n "k" #'evil-previous-visual-line
      :i "\C-k" #'end-of-visual-line
      :i "\C-h" #'delete-backward-char
      :i "\C-a" #'toggle-input-method
      :i "\M-j" #'yas-next-field-or-maybe-expand
      :i "\M-k" #'yas-prev-field
      :n "<SPC>gt" #'centaur-tabs-forward
      :n "<SPC>tn" #'centaur-tabs--create-new-tab
      :n "<SPC>sn" #'yas-new-snippet
)

(map! :map evil-org-mode-map
      :i "C-h" #'evil-org-delete-backward-char)

(map! :map LaTeX-mode-map
      :i "M-j" #'yas-next-field-or-maybe-expand
      :i "M-k" #'yas-prev-field
      )
(add-hook 'text-mode-hook (lambda()(company-mode -1)))
(add-hook 'text-mode-hook (lambda()(line-number-mode -1)))
;(setq telega-chat-fill-column 60)
(add-hook 'telega-load-hook 'telega-appindicator-mode)
(add-hook 'telega-load-hook 'visual-fill-column-mode--enable)
(add-hook 'org-mode-hook (lambda()(olivetti-mode t)))
                           ;(lambda()(line-number-mode nil)))
                           ;(lambda()(spell-fu-mode -1)))
;                           (lambda()(company-mode -1))
(add-hook 'markdown-mode-hook (lambda()(olivetti-mode t)))
(add-hook 'markdown-mode-hook (lambda()(display-line-numbers-mode -1)))
(plist-put! +ligatures-extra-symbols
            :я  nil )
; `load!' s one

;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.
