(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-names-vector
   ["#191919" "#FF5E5E" "#468800" "#E9FDAC" "#8CDAFF" "#C586C0" "#85DDFF" "#d4d4d4"])
 '(custom-safe-themes
   '("20f786e5d7b19abd8d93579a809976f2e477359a64ffdf36c4a07c6875045ece" "22c8110ceeade36347426926ae4adac25067743f49354fd04d9d49c051cb5149" "0e2a7e1e632dd38a8e0227d2227cb8849f877dd878afb8219cb6bcdd02068a52" "81e22639b3ed83056deb9a12313a2937f2008e5e2dc5918c9357e29fe9a3483f" "0fe24de6d37ea5a7724c56f0bb01efcbb3fe999a6e461ec1392f3c3b105cc5ac" "e3c64e88fec56f86b49dcdc5a831e96782baf14b09397d4057156b17062a8848" "75b8719c741c6d7afa290e0bb394d809f0cc62045b93e1d66cd646907f8e6d43" "990e24b406787568c592db2b853aa65ecc2dcd08146c0d22293259d400174e37" default))
 '(fci-rule-color "#515151")
 '(jdee-db-active-breakpoint-face-colors (cons "#171F24" "#FFFFFF"))
 '(jdee-db-requested-breakpoint-face-colors (cons "#171F24" "#468800"))
 '(jdee-db-spec-breakpoint-face-colors (cons "#171F24" "#777778"))
 '(objed-cursor-color "#FF5E5E")
 '(pdf-view-midnight-colors (cons "#d4d4d4" "#191919"))
 '(rustic-ansi-faces
   ["#191919" "#FF5E5E" "#468800" "#E9FDAC" "#8CDAFF" "#C586C0" "#85DDFF" "#d4d4d4"])
 '(safe-local-variable-values
   '((display-line-numbers-mode)
     (olivetti-body-width . 110)
     (flyspell-mode)))
 '(vc-annotate-background "#191919")
 '(vc-annotate-color-map
   (list
    (cons 20 "#468800")
    (cons 40 "#7caf39")
    (cons 60 "#b2d672")
    (cons 80 "#E9FDAC")
    (cons 100 "#efd98e")
    (cons 120 "#f5b671")
    (cons 140 "#FC9354")
    (cons 160 "#e98e78")
    (cons 180 "#d78a9c")
    (cons 200 "#C586C0")
    (cons 220 "#d8789f")
    (cons 240 "#eb6b7e")
    (cons 260 "#FF5E5E")
    (cons 280 "#dd6464")
    (cons 300 "#bb6a6b")
    (cons 320 "#997071")
    (cons 340 "#515151")
    (cons 360 "#515151")))
 '(vc-annotate-very-old-color nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(outline-1 ((t (:extend t :foreground "#d3869b" :weight bold :family "FantasqueSansMono Nerd Font Mono")))))
(put 'customize-group 'disabled nil)
