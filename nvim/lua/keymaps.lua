local map = vim.api.nvim_set_keymap
local default_opts = {noremap = true, silent = true}
-- map('n', '<Space>', '', {})
vim.g.mapleader = '\\'

function insertdate()
--    local inscurdate = os.date("# %d-%m-%Y %A") 
    vim.api.nvim_set_current_line(os.date("# %d.%m.%Y %A"))
end

map('n', 'j', 'gj', default_opts)
map('n', 'k', 'gk', default_opts) 
-- map('n', '<Tab>', ':BufferLineCycleNext<CR>', default_opts)
-- map('n', '<S-Tab>', ':BufferLineCyclePrev<CR>', default_opts)
map('n', '<leader>fr', ':Telescope oldfiles<CR>', default_opts)
map('n', '<leader>fm', ':e ~/FreeBSD_Sync/morning2021.md<CR>', default_opts)
map('n', '<leader>fs', ':w<CR>', default_opts)
map('n', '<leader>b', ':Telescope oldfiles<CR>', default_opts)
map('n', '<C-x>b', ':Telescope buffers<CR>', default_opts)
map('n', 'z=', ':Telescope spell_suggest<CR>', default_opts)
map('n', '<Space>', '<PageDown> zz', default_opts)
map('n', '<C-Space>', '<PageUp> zz', default_opts)
map('v', '<C-c>', '"+y', default_opts)
map('n', '<C-v>', '"+p', default_opts)
map('n', '<F11>', ':set spell!<CR>', default_opts)
map('i', '<F11>', '<C-O>:set spell!<CR>', default_opts)
map('i', '<C-\\>', '<C-^>', default_opts)
map('n', '<leader>n', ':NvimTreeToggle<CR>', default_opts)
map('i', '<C-l>', '<C-o>zz', default_opts)
map('n', '<leader>w', ':w<CR>', default_opts)
map('n', '<leader>tn', ':tabnew<CR>', default_opts)
vim.keymap.set('n', '<leader>id', function() insertdate() end, {noremap = true})
