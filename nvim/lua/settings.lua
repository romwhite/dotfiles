local cmd = vim.cmd
local exec = vim.api.nvim_exec
local g = vim.g
local opt = vim.opt

opt.spelllang={ 'en_us', 'ru' }
opt.number=true
opt.splitright=true
opt.splitbelow=true
opt.clipboard:append {unnamedplus}
opt.keymap='russian-jcukenwin'
-- opt.guifont='Iosevka:h18'
opt.termguicolors=true
cmd'colorscheme gruvbox'
cmd([[
filetype indent plugin on
syntax enable
]])
cmd([[set nospell]])
opt.expandtab=true
opt.shiftwidth=4
opt.softtabstop=4
opt.smartindent=true
opt.wrap=true
opt.iminsert=0
opt.imsearch=0
opt.foldcolumn='4'
opt.lbr=true
exec([[
augroup YankHighlight
autocmd!
autocmd TextYankPost * silent! lua vim.highlight.on_yank{higroup="IncSearch", timeout=700}
augroup end
]], false)
vim.g['loaded_netrw'] = 1
vim.g['loaded_netrwPlugin'] = 1

vim.g['pandoc#syntax#conceal#urls'] = 1
vim.g['pandoc#formatting#mode'] = 's' 
vim.g['pandoc#syntax#style#emphases'] = 1
vim.g['pandoc#syntax#codeblocks#embeds#langs'] = { "c", "sh", "tex", "vim" }
vim.g['pandoc#folding#fdc']=4
vim.g['vimtex_view_method'] = 'zathura'


-- vim.g['UltiSnipsExpantTrigger'] = '<tab>'
-- vim.g['UltiSnipsJumpForwardTrigger'] = '<c-j>'
-- vim.g['UltiSnipsJumpBackwardTrigger'] = '<c-k>'
-- vim.g['UltiSnipsSnippetDirectories'] = { "UltiSnips", "my_snippets" }
-- vim.g['completion_enable_snippet'] = 'UltiSnips'
require('lualine').setup()
require("bufferline").setup()
require('nvim-tree').setup{
    disable_netrw       = true,
                hijack_netrw        = true,
                open_on_tab         = false,
                hijack_cursor       = false,
                update_cwd          = false,
             
             view = {
                 width              = {
                     min = 30,
                     max = 40
                 },
                 side               = 'left',
                 number             = false,
                 relativenumber     = false,
                 signcolumn         = 'yes',
                 
             },
             filters = {
                 dotfiles           = false,
                 custom             = {}
             }
     }
require('startup').setup({theme="dashboard"})

local cmp = require'cmp'
cmp.setup({
    snippet = {
      -- REQUIRED - you must specify a snippet engine
      expand = function(args)
        -- require('luasnip').lsp_expand(args.body) -- For `luasnip` users.
      end,
    },
    window = {
       completion = cmp.config.window.bordered(),
       documentation = cmp.config.window.bordered(),
    },
    mapping = cmp.mapping.preset.insert({
      ['<C-b>'] = cmp.mapping.scroll_docs(-4),
      ['<C-f>'] = cmp.mapping.scroll_docs(4),
      ['<C-Space>'] = cmp.mapping.complete(),
      ['<C-e>'] = cmp.mapping.abort(),
      ['<CR>'] = cmp.mapping.confirm({ select = true }), 
  }),
    sources = cmp.config.sources({
       { name = 'luasnip' }, -- For luasnip users.
    }, {
 --     { name = 'buffer' },
    },
    {
        {name = 'emoji'}
    })
  })
  

  -- Set configuration for specific filetype.
  cmp.setup.filetype('gitcommit', {
    sources = cmp.config.sources({
      { name = 'git' }, -- You can specify the `git` source if [you were installed it](https://github.com/petertriho/cmp-git).
    }, {
      { name = 'buffer' },
    })
  })

  -- Use buffer source for `/` and `?` (if you enabled `native_menu`, this won't work anymore).
  cmp.setup.cmdline({ '/', '?' }, {
    mapping = cmp.mapping.preset.cmdline(),
    sources = {
      { name = 'buffer' }
    }
  })

  cmp.setup.cmdline(':', {
    mapping = cmp.mapping.preset.cmdline(),
    sources = cmp.config.sources({
      { name = 'path' }
    }, {
      { name = 'cmdline' }
    }),
    matching = { disallow_symbol_nonprefix_matching = false }
  })





