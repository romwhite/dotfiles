vim.cmd [[packadd packer.nvim]]

return require('packer').startup(function()
    -- Packer сам себя
    use 'wbthomason/packer.nvim'
    use 'morhetz/gruvbox'
    use 'tpope/vim-unimpaired'
    use { 'nvim-lualine/lualine.nvim',
        requires = {'nvim-tree/nvim-web-devicons', opt = true},
        config = function()
            require('lualine').setup{
     refresh = {
          statusline = 0,
          tabline = 1000,
          winbar = 1000,
        }
                }
            options = {theme = 'gruvbox'}
        end, }
        
        use {'akinsho/bufferline.nvim', requires = 'kyazdani42/nvim-web-devicons',
        config = function()
	end, }

        use {
            'nvim-tree/nvim-tree.lua',
            requires = {
            'nvim-tree/nvim-web-devicons', -- optional, for file icon
            },
                    }

    use 'vim-pandoc/vim-pandoc'
    use 'vim-pandoc/vim-pandoc-syntax'
--    use 'lervag/vimtex'
--    use 'neovim/nvim-lsp'
--    use 'neovim/nvim-lspconfig'
--    use 'hrsh7th/cmp-nvim-lsp'
--    use 'hrsh7th/cmp-buffer'
    use 'hrsh7th/cmp-path'
    use 'hrsh7th/cmp-cmdline'
    use 'hrsh7th/cmp-emoji'
    use 'hrsh7th/nvim-cmp'
 --   use 'quangnguyen30192/cmp-nvim-ultisnips'
    use 'nvim-lua/popup.nvim'
    use 'nvim-lua/plenary.nvim'
--    use 'nvim-treesitter/nvim-treesitter'
    use { 'nvim-telescope/telescope.nvim',
        requires = { {'nvim-lua/plenary.nvim'} },
        config = function() require'telescope'.setup {} end, }
    use 'tversteeg/registers.nvim'
    use {
      "startup-nvim/startup.nvim",
      requires = {"nvim-telescope/telescope.nvim", "nvim-lua/plenary.nvim"},
      config = function()
        require"startup".setup()
      end
    }
--    use 'SirVer/ultisnips'
end)

